package com.monitor.server.web.service.impl;

import org.springframework.stereotype.Service;

import com.monitor.server.web.service.FacadeService;


/**
 * 
 * @author LJ
 *
 */
@Service(value="facadeService")
public class FacadeServiceImpl implements FacadeService {

}
